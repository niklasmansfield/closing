import doctest
def closing_hour(weekday, holiday = False):
    """
    Returns the closing hours of a small store.


    Args: 
    weekday             1 equals monday, 7 is sunday.
    holiday             If the day in question is a holiday or not.
    
    >>> closing_hour(1)
    18
    >>> closing_hour(2)
    19
    >>> closing_hour(14)
    14

    """
    if weekday == 7 or holiday:
        return 14
    if weekday % 2:
        return 18
    else:
        return 19
doctest.testmod()
